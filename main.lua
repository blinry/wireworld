require "lib.slam"
vector = require "lib.hump.vector"
tlfres = require "lib.tlfres"
local class = require 'lib.middleclass' -- see https://github.com/kikito/middleclass

require "lib.helpers"

CANVAS_WIDTH = 1920
CANVAS_HEIGHT = 1080

GRID_WIDTH = 0
GRID_HEIGHT = 0

TILE_SIZE = 0

won = false
lost = false
message = nil

primaryTool = "wire"
autoplay = true
clearRadius = 0

frameRate = 1/(60/4)
ioWireLength = 5

fixedInputs = {}
currentOutputs = {}
expectedOutputs = {}
inputPositions = {}
outputPositions = {}

phase = "title"

function emptyLevel()
    level_height = 20
    level_width = math.floor(level_height*16.0/9)
    local levelDesc = ""
    for y=1,level_height do
        for x=1,level_width do
            levelDesc = levelDesc .. " "
        end
        levelDesc = levelDesc .. "\n"
    end
    return levelDesc
end

function defaultLevel(numInputs, numOutputs)
    local width = 25
    local height = math.max(math.max(numInputs, numOutputs)*7, 9)
    local wirelength = 0

    local levelDesc = ""
    -- place input wires at the left, ouptut at the right. input wires start with numbers 1,2,3... outputs with A,B,C...

    for y=1,height do
        for x=1,width do
            if y%7 == 4 then
                inputIndex = math.floor(y/7) + 1
                outputLetter = string.char(64+inputIndex)

                if x==1 then
                    if inputIndex <= numInputs then
                        levelDesc = levelDesc .. inputIndex
                    else
                        levelDesc = levelDesc .. " "
                    end
                elseif x<=wirelength+1 then
                    if inputIndex <= numInputs then
                        levelDesc = levelDesc .. "#"
                    else
                        levelDesc = levelDesc .. " "
                    end
                elseif x==width then
                    if inputIndex <= numOutputs then
                        levelDesc = levelDesc .. outputLetter
                    else
                        levelDesc = levelDesc .. " "
                    end
                elseif x>=width-wirelength then
                    if inputIndex <= numOutputs then
                        levelDesc = levelDesc .. "#"
                    else
                        levelDesc = levelDesc .. " "
                    end
                else
                    levelDesc = levelDesc .. " "
                end
            else
                levelDesc = levelDesc .. " "
            end
        end
        levelDesc = levelDesc .. "\n"
    end
    return levelDesc
end

level = nil
levels = {
    {
        title = "Wire",
        description = "Welcome to Wireworld! Use your mouse to connect the input to the output. Then press Return, to see the electrons flow.",
        level = [[



1####


                  ####A



]],
        inputs = {
            "#          ",
        },
        outputs = {
            "#          ",
        }
    },
    {
        title = "Desoldering",
        description = "Remove wires with your right mouse button. Repair the circuit so that only output B receives the signals! You can press Escape to restart the output.",
        level = [[


        #######A
        #  
        #
1########


           ####B


]],
        inputs = {
            "#     ",
        },
        outputs = {
            "      ",
            "#     ",
        }
    },
    {
        title = "Delay",
        description = "This time, output B should receive the input one step later than input A. Find a way to delay the signal! (Electrons also flow diagonally.) You can press Space to go step by step.",
        level = [[


              A

1

              B


]],
        inputs = {
            "#          ",
        },
        outputs = {
            "#          ",
            " #         ",
        }
    },
    {
        title = "Longer Delay",
        description = "What if we need a longer delay? The diagrams at the bottom show the pattern sent by the inputs, and the expected patterns of the outputs.",
        level = [[


              A

1

              B


]],
        once = true,
        inputs = {
            "#         ",
        },
        outputs = {
            "#         ",
            "          #",
        }
    },
    {
        title = "Really long Delay",
        description = "Management has decided that we need an even longer delay. Can you make it work?",
        level = [[


              A

1

              B


]],
        once = true,
        inputs = {
            "#          ",
        },
        outputs = {
            "#          ",
            "                          #",
        }
    },
    {
        title = "Duplication",
        description = "We're getting an important signal on the input! Make sure all outputs receive it at the same time!",
        level = [[


              A

1              


              B

        C
]],
        inputs = {
            "#   #       #   #   #        ",
        },
        outputs = {
            "#   #       #   #   #        ",
            "#   #       #   #   #        ",
            "#   #       #   #   #        ",
        }
    },
    {
        title = "Rules",
        description = "Here are the rules of Wireworld: In each step, all electron heads (orange) become electron tails (dark gray). All electron tails disappear. And all wires which have one or two electron heads next to them (including diagonally), become electron heads! This means that transmission stops when there are more than two electrons next to each other! Step through this level slowly (using space), and find out how to fix this level!",
        level = [[


   ##         #
1#######   #######A
       #  #   #
      ### #  ####
       #  #  #
        ##  ##


]],
        once = true,
        inputs = {
            "#"
        },
        outputs = {
            "#"
        }
    },
--    {
--        title = "OR Gate (Solution)",
--        description = "Here's the smallest possible OR gate.",
--        level = [[
--
--
--1#### 
--     #
--    #######A
--     #
--2#### 
--
--
--]],
--        inputs = {
--            "#     #                 #     ",
--            "#           #           #     ",
--        },
--        outputs = {
--            "#     #     #           #     ",
--        }
--    },
--    {
--        title = "Delay",
--        description = "Here, all input will eventually go to output A. As soon as the first electron arrives at any output, we start recording it. Make sure that output B receives the electron at the correct time!",
--        level = [[
--  
--  
--1             B
--#
--#
--A             
--  
--  
--]],
--        once = true,
--        inputs = {
--            "#",
--        },
--        outputs = {
--            "#                   ",
--            "                   #",
--        }
--    },

--    {
--        title = "Diode",
--        description = "Here's a diode! The signal from the left should be transmitted to the right, but not the other way around! But... it seems like the diode is turned the wrong way. Can you fix it?",
--        level = [[
--  
--  
--        ##      
--1     ## ###   B
--A       ##     2
--
--  
--]],
--        once = true,
--        inputs = {
--            "#   #",
--            "           #  #",
--        },
--        outputs = {
--            "#   #",
--            "           #  #      #   #",
--        }
--    },

    {
        title = "Clock",
        description = "We only get a single input electron. Find a way to output electrons in the correct intervals!",
        level = [[



     #
1#############A
     #   #
     #####



]],
        inputs = {
            "#                                    ",
        },
        outputs = {
            "#           ",
        }
    },
    {
        title = "Fast Clock",
        inputs = {
            "#                                    ",
        },
        outputs = {
            "#     ",
        }
    },
    {
        title = "Very fast clock",
        inputs = {
            "#                                ",
        },
        outputs = {
            "#   ",
        }
    },
    {
        title = "Fastest clock",
        inputs = {
            "#                                       ",
        },
        outputs = {
            "#  ",
        }
    },
    {
        title = "Diode",
        description = "Here, the output is connected to the input, so it will receive the electron once. We want to reroute the electron to the output a second time. Can this strange contraption help you with that?",
        level = [[
  
  
1                 
#
#           ##
A         ## ### 
            ##

  
]],
        once = true,
        inputs = {
            "#",
        },
        outputs = {
            "#             #",
        }
    },
    {
        title = "OR",
        description = "The output should receive an electron if either input 1 or input 2 outputs an electron. You'll need to find a way to prevent electrons from cancelling each other out.",
        level = [[


1     
      
           A
      
2     


]],
        inputs = {
            "#     #                 #     ",
            "#           #           #     ",
        },
        outputs = {
            "#     #     #           #     ",
        }
    },
    {
        title = "AND NOT",
        description = "Our scientists have invented a new gate, an AND NOT gate! It's supposed to output an electron if there's one coming from 1, but not from 2. Unfortunately, they forgot how to connect it properly. Can you find out?",
        level = [[


1

      #
     ###        A
      #

2


]],
        inputs = {
            "#                 #        #     ",
            "         #        #              ",
        },
        outputs = {
            "#                          #     ",
        }
    },
    {
        title = "AND",
        description = "You can use two AND NOT gates to build an AND gate!",
        inputs = {
            "#        #                 #     ",
            "#                 #        #     ",
        },
        outputs = {
            "#                          #     ",
        }
    },
    {
        title = "Intersection",
        description = "Here's your final challenge: Route signals from 1 to B, and from 2 to A!",
        inputs = {
            "#           #     #           ",
            "      #                 #     ",
        },
        outputs = {
            "      #                 #     ",
            "#           #     #           ",
        }
    },
    {
        title = "Thanks for playing!",
        description = "You can find games and music at blinry.org and winniehell.de. Press 0/9 to cycle through all levels! And if you want to have your mind blown, search the Internet for the  'quinapalus Wireworld Computer'!",
        level = [[


    ###   ###
   #   # #   #
  #     #     #
1##           ##A
  #           #
   #         #
    #       #
     #     #
      #   #
       # #
        #


]],
        inputs = {
            "#  "
        },
        outputs = {
            "#  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #",
        }
    },
}

function magiclines(s)
    if s:sub(-1)~="\n" then s=s.."\n" end
    return s:gmatch("(.-)\n")
end

function loadLevel(i)
    won = false
    lost = false
    message = nil
    currentLevel = i
    undoStack = {}
    autoplay = false
    level = levels[i]

    frame = 0
    clock = 0

    inputPositions = {}
    outputPositions = {}

    local levelDesc = level.level or defaultLevel(#level.inputs, #level.outputs)

    GRID_WIDTH = 0
    GRID_HEIGHT = 0
    for line in magiclines(levelDesc) do
        GRID_WIDTH = math.max(GRID_WIDTH, #line)
        GRID_HEIGHT = GRID_HEIGHT + 1
    end

    grid = {}
    for x=1,GRID_WIDTH do
        grid[x] = {}
        for y=1,GRID_HEIGHT do
            grid[x][y] = {wire="empty", electron="empty"}
        end
    end
    y = 1
    for line in magiclines(levelDesc) do
        for x=1,#line do
            local c = line:sub(x,x)
            if c == " " then
                --grid[x][y] = {type="empty"}
            elseif c == "#" then
                grid[x][y].wire = "wire"
            elseif c == "h" then
                grid[x][y].wire = "wire"
                grid[x][y].electron = "head"
            elseif c == "t" then
                grid[x][y].wire = "wire"
                grid[x][y].electron = "tail"
            elseif c:match("%d") then
                grid[x][y].wire = "wire"
                grid[x][y].electron = "empty"
                local inputIndex = c:byte() - string.byte("0")
                --print(inputIndex)
                inputPositions[inputIndex] = {x=x, y=y}
            elseif c:match("%u") then
                grid[x][y].wire = "wire"
                grid[x][y].electron = "empty"
                local outputIndex = c:byte() - string.byte("A") + 1
                --print(outputIndex)
                outputPositions[outputIndex] = {x=x, y=y}
            end
        end
        y = y + 1
    end
    --print(inputPositions[1])

    fixedInputs = {}

    if level.inputs then
        for i=1,#level.inputs do
            fixedInputs[i] = ""
            local repeats
            if level.once then
                repeats = 1
            else
                repeats = 10
            end
            for j=1,repeats do
                fixedInputs[i] = fixedInputs[i] .. level.inputs[i]
            end
            -- fill up to 30 characters
            while #fixedInputs[i] < 30 do
                fixedInputs[i] = fixedInputs[i] .. " "
            end
            -- limit to 30 characters
            fixedInputs[i] = fixedInputs[i]:sub(1,30)
        end
    else
        level.inputs = {}
    end

    currentOutputs = {}
    expectedOutputs = {}
    if level.outputs then
        for i=1,#level.outputs do
            currentOutputs[i] = ""
            expectedOutputs[i] = ""
            local repeats = 10
            if level.once then
                repeats = 1
            end
            for j=1,repeats do
                expectedOutputs[i] = expectedOutputs[i] .. level.outputs[i]
            end
            -- fill up to 30 characters
            while #expectedOutputs[i] < 30 do
                expectedOutputs[i] = expectedOutputs[i] .. " "
            end
            -- limit to 30 characters
            expectedOutputs[i] = expectedOutputs[i]:sub(1,30)
        end
    else
        level.outputs = {}
    end

    topBarHeight = 230
    bottomBarHeight = #level.inputs + (#level.outputs)*3 + 2 -- tiles

    TILE_SIZE = math.min(CANVAS_WIDTH / GRID_WIDTH, (CANVAS_HEIGHT - topBarHeight) / (GRID_HEIGHT + bottomBarHeight))

    techFont = love.graphics.newFont("fonts/whiterabbit.ttf", TILE_SIZE*0.8)

    setInputs()
    setOutputs()

    undoStack = {}
    saveState()
end

function nextLevel()
    currentLevel = (currentLevel % #levels) + 1
    loadLevel(currentLevel)
end

function prevLevel()
    currentLevel = (currentLevel - 2) % #levels + 1
    loadLevel(currentLevel)
end

undoStack = {}

function undo()
    if #undoStack > 0 then
        local state = undoStack[#undoStack]
        grid = state.grid
        currentOutputs = state.currentOutputs
        clock = state.clock
        table.remove(undoStack)
        lost = false
        won = false
        message = nil
    end
end

function stop()
    autoplay = false
    lost = false
    won = false
    clock = 0
    frame = 0
    message = nil
    clear()

    for i=1,#level.outputs do
        currentOutputs[i] = ""
    end
end

function reset()
    stop()

    if #undoStack > 0 then
        local state = undoStack[1]
        grid = state.grid
        undoStack = {}
        saveState()
    end
end

function saveState()
    table.insert(undoStack, {grid=deepcopy(grid), currentOutputs=deepcopy(currentOutputs), clock=clock})
end

function love.load()
    -- set up default drawing options
    love.graphics.setBackgroundColor(0, 0, 0)

    -- Hide mouse cursor.
    --love.mouse.setVisible(false)

    -- load assets
    images = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("images")) do
        if filename ~= ".gitkeep" then
            images[filename:sub(1,-5)] = love.graphics.newImage("images/"..filename)
        end
    end

    sounds = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("sounds")) do
        if filename ~= ".gitkeep" then
            sounds[filename:sub(1,-5)] = love.audio.newSource("sounds/"..filename, "static")
            sounds[filename:sub(1,-5)]:setVolume(0.2)
        end
    end

    music = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("music")) do
        if filename ~= ".gitkeep" then
            music[filename:sub(1,-5)] = love.audio.newSource("music/"..filename, "stream")
            music[filename:sub(1,-5)]:setLooping(true)
        end
    end
    theMusic = music["elektrische-entspannung"]
    theMusic:setVolume(0.5)
    theMusic:play()

    titleFont = love.graphics.newFont("fonts/supercomputer.ttf", 70)
    titleFontBig = love.graphics.newFont("fonts/supercomputer.ttf", 250)
    defaultFont = love.graphics.newFont("fonts/rubik.ttf", 30)
    defaultFontBig = love.graphics.newFont("fonts/rubik.ttf", 70)
    techFont = love.graphics.newFont("fonts/whiterabbit.ttf", 30)

    love.graphics.setFont(defaultFont)

    loadLevel(1)
end

prev_x = nil
prev_y = nil
frame = 0
clock = 0
function love.update(dt)
    local mx, my = love.mouse.getPosition()
    local tile_x = math.floor(mx / TILE_SIZE) + 1
    local tile_y = math.floor((my - topBarHeight) / TILE_SIZE) + 1

    if tile_x >= 1 and tile_x <= GRID_WIDTH and tile_y >= 1 and tile_y <= GRID_HEIGHT then
        if prev_x == nil then
            prev_x = tile_x
            prev_y = tile_y
        end

        if not won then
            if love.mouse.isDown(1) then
                if clock ~= 0 then
                    stop()
                end
                setLine(prev_x, prev_y, tile_x, tile_y, {wire=primaryTool, electron="empty"})
            elseif love.mouse.isDown(2) then
                if clock ~= 0 then
                    stop()
                end
                setCircle(tile_x, tile_y, {wire="empty", electron="empty"}, clearRadius)
            end
        end

        --setInputs()
        --setOutputs()

        prev_x = tile_x
        prev_y = tile_y
    end

    if autoplay then
        frame = frame+dt
    elseif love.keyboard.isDown("space") then
        frame = frame+dt*0.35
    end
    if frame > frameRate then
        step()
        frame = 0
    end
end

function setInputs()
    for i, input in ipairs(fixedInputs) do
        local char

        local pos = inputPositions[i]

        if clock <= #input then
            char = input:sub(clock, clock)
        else
            char = " "
        end

        if char == "#" then
            setTile(pos.x, pos.y, {electron="head"})
            sounds.blip:setPitch(1 + math.random()*0.5)
            sounds.blip:play()
        else
            --setTile(pos.x, pos.y, {electron="empty"})
        end
    end

    --dist = 6
    --local x = 1
    --local y = dist
    --for i, input in ipairs(fixedInputs) do
    --    local char
    --    if clock <= #input then
    --        char = input:sub(clock, clock)
    --    else
    --        char = " "
    --    end

    --    if char == "#" then
    --        setTile(x, y, {electron="head"})
    --    else
    --        setTile(x, y, {electron="empty"})
    --    end

    --    local width = ioWireLength
    --    for x = 1, width do
    --        local wire
    --        if x == 1 then
    --            wire = "wire"
    --        else
    --            wire = "wire"
    --        end

    --        setTile(x, y, {wire=wire})
    --    end

    --    y = y+dist
    --end
end

function setOutputs()
    --local dist = 6
    --local x = GRID_WIDTH
    --local y = dist
    --local width = ioWireLength

    --for i, output in ipairs(levels[currentLevel].outputs) do
    --    for x = GRID_WIDTH-width+1, GRID_WIDTH do
    --        local wire
    --        if x == GRID_WIDTH then
    --            wire = "wire"
    --        else
    --            wire = "wire"
    --        end

    --        setTile(x, y, {wire=wire})
    --    end

    --    y = y+dist
    --end
end

function recordOutputs()
    local dist = 6

    local hadOutput = false
    for i, output in ipairs(currentOutputs) do
        local pos = outputPositions[i]
        local x = pos.x
        local y = pos.y

        local outputOn = getTile(x, y).electron == "head"

        if output ~= "" or outputOn then
            hadOutput = true
        end
    end

    for i, output in ipairs(levels[currentLevel].outputs) do
        local pos = outputPositions[i]
        local x = pos.x
        local y = pos.y

        -- record output as a string, as soon as first electron comes in
        local outputOn = getTile(x, y).electron == "head"

        if outputOn or hadOutput then
            hadOutput = true
            if outputOn then
                currentOutputs[i] = currentOutputs[i].."#"
                sounds.blip2:setPitch(1 + math.random()*0.5)
                sounds.blip2:play()
            else
                currentOutputs[i] = currentOutputs[i].." "
            end
            -- cut to length of 30 (from beginning)
            if #currentOutputs[i] > 30 then
                currentOutputs[i] = currentOutputs[i]:sub(1, 30)
            end
        end
    end
end

function checkWin()
    -- check whether fixedInputs and currentOutputs are identical, pairwise
    for i, _ in ipairs(expectedOutputs) do
        if expectedOutputs[i] ~= currentOutputs[i] then
            won = false
            return
        end
    end
    if not won then
        sounds.win:play()
    end
    won = true
end

function step()
    saveState()

    clock = clock + 1

    -- Wireworld rules
    local grid_copy = deepcopy(grid)
    for x=1,GRID_WIDTH do
        for y=1,GRID_HEIGHT do
            local tile = grid[x][y]
            if tile.electron == "head" then
                grid_copy[x][y].electron = "tail"
            elseif tile.electron == "tail" then
                grid_copy[x][y].electron = "empty"
            elseif tile.electron == "empty" and tile.wire ~= "empty" then
                local count = 0
                for dx=-1,1 do
                    for dy=-1,1 do
                        if dx ~= 0 or dy ~= 0 then
                            local tile = getTile(x+dx, y+dy)
                            if tile.electron == "head" then
                                count = count + 1
                            end
                        end
                    end
                end
                if count == 1 or count == 2 then
                    grid_copy[x][y].electron = "head"
                end
            end
        end
    end
    grid = grid_copy

    setInputs()
    setOutputs()

    recordOutputs()
    audio()
    checkWin()
end

function audio()
    -- for each head, play meow sound, if wire is bell
    -- the further up, the louder
    -- the more right, the higher

    for x=1,GRID_WIDTH do
        for y=1,GRID_HEIGHT do
            local tile = grid[x][y]
            if tile.electron == "head" and tile.wire == "bell" then
                local pitch = 1 + (y / GRID_WIDTH)
                --local volume = 1 - (x / GRID_HEIGHT)
                meow = sounds.meow:play()
                meow:setPitch(pitch)
                --meow:setVolume(volume)
            end
        end
    end
end

function love.mouse.getPosition()
    return tlfres.getMousePosition(CANVAS_WIDTH, CANVAS_HEIGHT)
end

function love.keypressed(key)
    if key == "f" then
        isFullscreen = love.window.getFullscreen()
        love.window.setFullscreen(not isFullscreen)
    elseif key == "m" then
        if musicVolume == 0 then
            musicVolume = 0.5
        else
            musicVolume = 0
        end
        theMusic:setVolume(musicVolume)
    end

    if phase == "title" then
        if key == "space" then
            phase = "game"
        end
    elseif phase == "game" then
        if won then
            if key == "space" then
                nextLevel()
            end
        else
            if key == "escape" then stop()
            elseif key == "r" then
                reset()
            elseif key == "z" or key == "u" or key == "backspace" then
                undo()
            elseif key == "return" then
                toggle()
            elseif key == "c" then
                clear()
            elseif key == "space" then
                autoplay = false
                frame = 0
                step()
            elseif key == "9" then
                prevLevel()
            elseif key == "0" then
                nextLevel()
            end
        end
    end
end

function toggle()
    if not autoplay then
        stop()
    end
    autoplay = not autoplay
    --clock = 0
    --if #level.inputs > 0 then
    --    clear()
    --end
    --frame = 0

    --if autoplay then
    --    for i, output in ipairs(currentOutputs) do
    --        currentOutputs[i] = ""
    --    end
    --end
end

function getTile(x, y)
    if x < 1 or x > GRID_WIDTH or y < 1 or y > GRID_HEIGHT then
        return {wire="empty", electron="empty"}
    end
    return grid[x][y]
end

function setTile(x, y, tile)
    if x < 1 or x > GRID_WIDTH or y < 1 or y > GRID_HEIGHT then
        return
    end

    -- merge properties
    for k,v in pairs(tile) do
        grid[x][y][k] = v
    end
end

function clear()
    -- clear all electrons
    for x=1,GRID_WIDTH do
        for y=1,GRID_HEIGHT do
            grid[x][y].electron = "empty"
        end
    end
end

function setLine(x1, y1, x2, y2, tile)
    -- Bresenham's line algorithm
    local dx = math.abs(x2 - x1)
    local dy = math.abs(y2 - y1)
    local sx = x1 < x2 and 1 or -1
    local sy = y1 < y2 and 1 or -1
    local err = dx - dy

    while true do
        if x1 > 1 and x1 < GRID_WIDTH and y1 > 1 and y1 < GRID_HEIGHT then
            if getTile(x1, y1).wire == "empty" then
                sounds.put:play()
            end
            setTile(x1, y1, deepcopy(tile))
        end

        if x1 == x2 and y1 == y2 then
            break
        end

        local e2 = 2 * err
        if e2 > -dy then
            err = err - dy
            x1 = x1 + sx
        end
        if e2 < dx then
            err = err + dx
            y1 = y1 + sy
        end
    end
end

function setCircle(x, y, tile, radius)
    radius = radius or 2
    for i=x-radius,x+radius do
        for j=y-radius,y+radius do
            if (i-x)*(i-x) + (j-y)*(j-y) <= radius*radius then
                if x > 1 and x < GRID_WIDTH and y > 1 and y < GRID_HEIGHT then
                    if getTile(i, j).wire ~= "empty" then
                        sounds.remove:play()
                    end
                    setTile(i, j, deepcopy(tile))
                end
            end
        end
    end
end

function love.keyreleased(key)
end

function love.mousepressed(x, y, button)
    saveState()
end

function love.wheelmoved(x, y)
    --clearRadius = clearRadius + y
    --if clearRadius < 0 then
    --    clearRadius = 0
    --end
end

function love.draw()
    local pcbGreen = {0, 0.2, 0}
    local spaceBlue = {0.5, 0.5, 1}
    local lightSpaceBlue = {0.7, 0.7, 1}
    local darkBitColor = {0.2, 0.2, 0.2}
    local orange = {1, 0.7, 0.1}

    love.graphics.setColor(1, 1, 1)
    tlfres.beginRendering(CANVAS_WIDTH, CANVAS_HEIGHT)

    if phase == "title" then
        love.graphics.draw(images.background, 0, 0)

        -- transform: rotate around center
        love.graphics.push()
        love.graphics.translate(CANVAS_WIDTH/2, CANVAS_HEIGHT/2)
        love.graphics.rotate(-0.144)
        love.graphics.translate(-CANVAS_WIDTH/2, -CANVAS_HEIGHT/2)

        love.graphics.setFont(titleFontBig)
        love.graphics.setColor(orange)
        love.graphics.printf("Wireworld", 0, 180, CANVAS_WIDTH, "center")

        love.graphics.setFont(defaultFontBig)
        love.graphics.setColor(lightSpaceBlue)
        love.graphics.printf("made by blinry & winniehell\nin 72 hours\nfor Ludum Dare 53", 0, 500, CANVAS_WIDTH, "center")
        love.graphics.printf("Press Space to start!", 0, 900, CANVAS_WIDTH, "center")

        love.graphics.pop()

    elseif phase == "game" then
        local border = 20

        love.graphics.setFont(techFont)

        -- draw level
        for x=1,GRID_WIDTH do
            for y=1,GRID_HEIGHT do
                if grid[x][y].wire == "empty" then
                    if x > 1 and x < GRID_WIDTH and y > 1 and y < GRID_HEIGHT then
                        -- pcb green
                        love.graphics.setColor(pcbGreen)
                    else
                        -- transparent
                        love.graphics.setColor(0, 0, 0, 0)
                    end
                    local offsetY = TILE_SIZE*0.1
                    love.graphics.rectangle("fill", (x-1)*TILE_SIZE, (y-1)*TILE_SIZE + topBarHeight + offsetY, TILE_SIZE, TILE_SIZE)
                end
            end
        end

        -- draw a thin grid on top of everything
        for x=2,GRID_WIDTH-1 do
            for y=2,GRID_HEIGHT-1 do
                love.graphics.setLineWidth(1)
                love.graphics.setColor(0, 0, 0, 0.25)
                local offsetY = TILE_SIZE*0.1
                love.graphics.rectangle("line", (x-1)*TILE_SIZE, (y-1)*TILE_SIZE + topBarHeight + offsetY, TILE_SIZE, TILE_SIZE)
            end
        end

        -- pass for shadow for wires. Darker versions of colors.
        for x=1,GRID_WIDTH do
            for y=1,GRID_HEIGHT do
                local offsetX = TILE_SIZE*0
                local offsetY = TILE_SIZE*0.1
                if grid[x][y].electron == "head" then
                    -- bright orange
                    love.graphics.setColor(0.75, 0.5, 0.25)
                    love.graphics.rectangle("fill", (x-1)*TILE_SIZE + offsetX, (y-1)*TILE_SIZE + topBarHeight + offsetY, TILE_SIZE, TILE_SIZE)
                elseif grid[x][y].electron == "tail" then
                    -- darker gray
                    love.graphics.setColor(0.1, 0.1, 0.1)
                    love.graphics.rectangle("fill", (x-1)*TILE_SIZE + offsetX, (y-1)*TILE_SIZE + topBarHeight + offsetY, TILE_SIZE, TILE_SIZE)
                elseif grid[x][y].wire == "wire" then
                    love.graphics.setColor(0.25, 0.25, 0.25)
                    love.graphics.rectangle("fill", (x-1)*TILE_SIZE + offsetX, (y-1)*TILE_SIZE + topBarHeight + offsetY, TILE_SIZE, TILE_SIZE)
                end
            end
        end

        -- pass for wire
        for x=1,GRID_WIDTH do
            for y=1,GRID_HEIGHT do
                if grid[x][y].electron == "head" then
                    -- bright orange
                    love.graphics.setColor(orange)
                    love.graphics.rectangle("fill", (x-1)*TILE_SIZE, (y-1)*TILE_SIZE + topBarHeight, TILE_SIZE, TILE_SIZE)
                elseif grid[x][y].electron == "tail" then
                    -- dark gray
                    love.graphics.setColor(0.3, 0.3, 0.3)
                    love.graphics.rectangle("fill", (x-1)*TILE_SIZE, (y-1)*TILE_SIZE + topBarHeight, TILE_SIZE, TILE_SIZE)
                elseif grid[x][y].wire == "wire" then
                    --wire
                    love.graphics.setColor(0.5, 0.5, 0.5)
                    love.graphics.rectangle("fill", (x-1)*TILE_SIZE , (y-1)*TILE_SIZE + topBarHeight , TILE_SIZE, TILE_SIZE)
                end
            end
        end

        -- print labels at input and outputPositions
        for i=1,#inputPositions do
            local x = inputPositions[i].x
            local y = inputPositions[i].y
            love.graphics.setColor(0, 0, 0)
            local number = i
            love.graphics.print(number, (x-1)*TILE_SIZE + TILE_SIZE/4 , (y-1)*TILE_SIZE + TILE_SIZE/4  + topBarHeight)
        end
        for i=1,#outputPositions do
            local x = outputPositions[i].x
            local y = outputPositions[i].y
            love.graphics.setColor(0, 0, 0)
            local letter = string.char(64+i)
            love.graphics.print(letter, (x-1)*TILE_SIZE + TILE_SIZE/4, (y-1)*TILE_SIZE + TILE_SIZE/4  + topBarHeight)
        end
        local mx, my = love.mouse.getPosition()
        -- draw arrow-shaped cursor
        --if primaryTool == "wire" then
        --    love.graphics.setColor(0.5, 0.5, 0.5)
        --elseif primaryTool == "bell" then
        --    love.graphics.setColor(1, 0.5, 0)
        --end
        --local size = 50
        --love.graphics.polygon("fill", mx, my, mx+size, my, mx+size, my+size, mx+size*2, my+size*0.5, mx+size, my-size*0.5, mx+size, my, mx, my)

        -- draw clearradius circle
        if love.mouse.isDown(2) then
            love.graphics.setColor(1, 1, 1)
            love.graphics.circle("line", mx, my, clearRadius*TILE_SIZE)
        end

        ---- print autoplay state
        --love.graphics.setColor(1, 1, 1)
        --if autoplay then
        --    love.graphics.print("Autoplay: On", 10, 30)
        --else
        --    love.graphics.print("Autoplay: Off", 10, 30)
        --end

        local sideBar = TILE_SIZE
        -- draw fixed inputs
        for i=1,#level.inputs do
            love.graphics.push()
            love.graphics.translate(sideBar, 0)
            local input = fixedInputs[i]
            for j=1,#input do
                local char = input:sub(j,j)
                if char == "#" then
                    -- same orange
                    love.graphics.setColor(orange)
                else
                    love.graphics.setColor(darkBitColor)
                end
                local x = (j-1)*TILE_SIZE
                local y = (GRID_HEIGHT + 1)*TILE_SIZE + (i-1)*TILE_SIZE
                love.graphics.rectangle("fill", x, y + topBarHeight, TILE_SIZE, TILE_SIZE)
                -- draw thin outline
                love.graphics.setLineWidth(1)
                love.graphics.setColor(0, 0, 0, 0.25)
                love.graphics.rectangle("line", x, y + topBarHeight, TILE_SIZE, TILE_SIZE)
            end
            love.graphics.pop()
            love.graphics.setColor(1, 1, 1)
            love.graphics.print(i, 10, 10 + topBarHeight + (GRID_HEIGHT + 1)*TILE_SIZE + (i-1)*TILE_SIZE)
        end

        local y_output_start = (GRID_HEIGHT + 1)*TILE_SIZE + (#level.inputs)*TILE_SIZE + TILE_SIZE + topBarHeight

        for i=1,#level.outputs do
            love.graphics.push()
            love.graphics.translate(sideBar, 0)
            -- draw expected outputs
            local output = expectedOutputs[i]

            local y_start = y_output_start + 3*(i-1)*TILE_SIZE
            local x_start = 0

            local y = y_start

            for j=1,#output do
                local char = output:sub(j,j)
                if char == "#" then
                    -- same orange
                    love.graphics.setColor(orange)
                else
                    love.graphics.setColor(darkBitColor)
                end
                local x = x_start + (j-1)*TILE_SIZE
                love.graphics.rectangle("fill", x, y, TILE_SIZE, TILE_SIZE)
                -- draw thin outline
                love.graphics.setLineWidth(1)
                love.graphics.setColor(0, 0, 0, 0.25)
                love.graphics.rectangle("line", x, y, TILE_SIZE, TILE_SIZE)
            end
            love.graphics.setColor(1, 1, 1)
            local letter = string.char(64+i)
            love.graphics.pop()
            love.graphics.print(letter, x_start + 10, 10 + y_start)

            love.graphics.push()
            love.graphics.translate(sideBar, 0)
            -- draw current outputs
            local output = currentOutputs[i]
            for j=1,#output do
                local char = output:sub(j,j)

                local color
                if char == "#" then
                    color = {1, 0.7, 0.1}
                else
                    color = darkBitColor
                end

                local x = x_start + (j-1)*TILE_SIZE
                local y = y_start + TILE_SIZE

                love.graphics.setColor(unpack(color))
                love.graphics.rectangle("fill", x, y, TILE_SIZE, TILE_SIZE)

                -- draw thin outline
                love.graphics.setLineWidth(1)
                love.graphics.setColor(0, 0, 0, 0.25)
                love.graphics.rectangle("line", x, y, TILE_SIZE, TILE_SIZE)
            end

            for j=1,#output do
                local char = output:sub(j,j)
                local x = x_start + (j-1)*TILE_SIZE
                local y = y_start + TILE_SIZE
                -- cross on top if wrong
                if char ~= expectedOutputs[i]:sub(j,j) then
                    love.graphics.setColor(1, 0, 0)
                    love.graphics.setLineWidth(TILE_SIZE/5)
                    love.graphics.line(x, y, x+TILE_SIZE, y+TILE_SIZE)
                    love.graphics.line(x+TILE_SIZE, y, x, y+TILE_SIZE)
                end
            end

            love.graphics.pop()
            love.graphics.setColor(1, 1, 1)
            local letter = string.char(64+i)
            love.graphics.print("?", x_start + 10, y_start + TILE_SIZE + 10)
        end

        love.graphics.push()
        love.graphics.translate(sideBar, 0)
        -- rectangle around current clock
        if #fixedInputs > 0 then
            if clock > 0 then
                love.graphics.setColor(spaceBlue)
                local x
                if clock <= #fixedInputs[1] then
                    x = (clock-1)*TILE_SIZE
                    if x >= 0 then
                        love.graphics.setLineWidth(TILE_SIZE/10)
                        love.graphics.rectangle("line", x, topBarHeight + (GRID_HEIGHT + 1)*TILE_SIZE, TILE_SIZE, TILE_SIZE*(#level.inputs))
                    end
                end
            end
        end

        -- rectangle around all output bits
        if #level.outputs > 0 then
            local x = #currentOutputs[1] - 1
            if x >= 0 then
                love.graphics.setColor(spaceBlue)
                love.graphics.setLineWidth(TILE_SIZE/10)
                love.graphics.rectangle("line", x*TILE_SIZE, y_output_start, TILE_SIZE, TILE_SIZE*(#level.outputs*3-1))
            end
        end

        love.graphics.pop()

        -- draw top box
        --love.graphics.setColor(0.8, 0.8, 0.8)
        --love.graphics.rectangle("fill", 0, 0, CANVAS_WIDTH, topBarHeight)

        -- print level title
        if level then
            love.graphics.setFont(titleFont)
            love.graphics.setColor(spaceBlue)
            love.graphics.print("Level " .. currentLevel .. ": " .. level.title, border, border)
        end

        love.graphics.setFont(defaultFont)

        -- print level description
        if level.description then
            love.graphics.setColor(1, 1, 1)
            love.graphics.printf(level.description, border, 110, CANVAS_WIDTH - 2*border)
        end

        -- autoplay marker in top right
        love.graphics.setColor(spaceBlue)
        local offset = 150
        if autoplay then
            love.graphics.draw(images.play, CANVAS_WIDTH - offset, CANVAS_HEIGHT - offset, 0, 0.5, 0.5)
        else
            --if clock == 0 then
            --    love.graphics.draw(images.stop, CANVAS_WIDTH - offset, CANVAS_HEIGHT - offset, 0, 0.5, 0.5)
            --else
            --    love.graphics.draw(images.pause, CANVAS_WIDTH - offset, CANVAS_HEIGHT - offset, 0, 0.5, 0.5)
            --end
        end

        if won then
            -- rounded transparent rectangle in the middle
            love.graphics.setColor(1, 1, 1, 0.5)
            local gap = CANVAS_WIDTH/4
            love.graphics.rectangle("fill", gap, gap, CANVAS_WIDTH - 2*gap, CANVAS_HEIGHT - 2*gap, 10, 10)
            love.graphics.setColor(0, 0, 0)
            love.graphics.printf("Well done! Press space to go to the next level.", gap+10, gap+35, CANVAS_WIDTH - 2*gap - 20, "center")
        end
        if message then
            love.graphics.setColor(1, 1, 1)
            love.graphics.print(message, 10, 10)
        end
        if lost then
            love.graphics.setColor(1, 0, 0)
            love.graphics.print("You lost!", 10, 120)
        end
    end

    tlfres.endRendering()
end
