# Wireworld

Made for Ludum Dare 53 in 72 hours by blinry and winniehell.

## Credits and license

Fonts used:

- Supercomputer (CC BY-SA 3.0)
- White Rabbit (MIT License)
- Rubik (Open Font License)

The soundtrack is released under CC BY 4.0.

The source code of the game is released under the [Blue Oak Model License 1.0.0](LICENSE.md) – a [modern alternative](https://writing.kemitchell.com/2019/03/09/Deprecation-Notice.html) to the MIT license. It's a a pleasant read! :)
